const express = require('express');
const mongoose = require('mongoose');

const authRouter = require('./routers/authRoute');
const userRouter = require('./routers/userRoute');
const loadRouter = require('./routers/loadRoute');
const truckRouter = require('./routers/truckRoute');

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/loads', loadRouter);
app.use('/api/trucks', truckRouter);

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://ihor:igor123@cluster0.wzqfq.mongodb.net/uber', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
      console.log(`Server is running on PORT:${PORT}`);
    });
  } catch (error) {
    console.log(error);
  }
};

start();
