const sizeOfTruck = require('../helpers/truck_types');

const Truck = require('../models/truck');
const User = require('../models/user');

module.exports.addTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User is not a driver'});
  }

  const {name, type} = req.body;
  const [width, length, height, payload] = sizeOfTruck(type);

  const truck = new Truck({
    name,
    created_by: user._id,
    status: 'IS',
    type,
    width,
    length,
    height,
    payload,
    assigned_to: null,
  });

  await truck.save();

  res.status(200).json({
    message: 'Truck created successfully',
  });
};

module.exports.getTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User is not a driver'});
  }

  const trucks = await Truck.find({created_by: req.user._id}, [
    '_id',
    'created_by',
    'assigned_to',
    'type',
    'status',
    'created_date',
  ]);

  res.status(200).json({trucks});
};

module.exports.getTruckById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User is not a driver'});
  }

  const truck = await Truck.findById(req.params.id, {__v: 0});

  if (!truck) {
    return res.status(400).json({message: 'Truck not found'});
  }

  res.status(200).json({truck});
};

module.exports.updateTruckById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User is not a driver'});
  }

  const truck = await Truck.findById(req.params.id, {__v: 0});

  if (!truck) {
    return res.status(400).json({message: 'Truck not found'});
  }

  if (user._id.toString() !== truck.created_by.toString()) {
    return res.status(400).json({message: 'You do not have such a truck'});
  }

  const type = req.body.type;
  const [width, length, height, payload] = sizeOfTruck(type);

  truck.type = type;
  truck.width = width;
  truck.length = length;
  truck.height = height;
  truck.payload = payload;

  await truck.save();

  res.status(200).json({message: 'Truck was updated successfully!'});
};

module.exports.deleteTruckById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User is not a driver'});
  }

  const truck = await Truck.findById(req.params.id, {__v: 0});

  if (!truck) {
    return res.status(400).json({message: 'Truck not found'});
  }

  if (user._id.toString() !== truck.created_by.toString()) {
    return res.status(400).json({message: 'You do not have such a truck'});
  }

  await Truck.findByIdAndRemove(req.params.id);

  res.status(200).json({message: 'Truck deleted successfully!'});
};

module.exports.assignTruckById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User is not a driver'});
  }

  const truck = await Truck.findById(req.params.id, {__v: 0});

  if (!truck) {
    return res.status(400).json({message: 'Truck not found'});
  }

  if (user._id.toString() !== truck.created_by.toString()) {
    return res.status(400).json({message: 'You do not have such a truck'});
  }

  const trucks = await Truck.find({created_by: req.user._id}, 'assigned_to');

  trucks.forEach((item) => {
    if (item.assigned_to) {
      return res.status(200).json({message: 'You have already assigned truck'});
    }
  });

  truck.assigned_to = req.user._id;

  await truck.save();

  res.status(200).json({message: 'Truck assigned successfully'});
};
