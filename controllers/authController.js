const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config.js');

module.exports.register = async (req, res) => {
  const {email, password, role} = req.body;

  if (email && password && role) {
    let user = await User.findOne({email});

    if (!user) {
      user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role: role.toUpperCase(),
      });
      await user.save();

      res.status(200).json({message: 'User created successfully'});
    } else {
      res.status(400).json({message: 'User is already exist'});
    }
  } else {
    res.status(400).json({message: 'Enter all fields'});
  }
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400)
        .json({message: `No user with email '${email}' found!`});
  }

  if (!password) {
    return res.status(400).json({message: `Enter your password`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password`});
  }

  const jwtToken = jwt.sign(
      {email, _id: user._id, created_date: user.created_date},
      JWT_SECRET);

  res.status(200).json({
    message: 'Success',
    jwt_token: jwtToken,
  });
};

module.exports.forgotPassword = (req, res) => {
  const {email} = req.body;
  const user = User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: 'Email is not registered.',
    });
  }

  res.status(200).json({message: 'New password sent to your email address'});
};
