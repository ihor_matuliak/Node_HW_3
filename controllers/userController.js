const User = require('../models/user');
const bcrypt = require('bcrypt');

module.exports.getUserInfo = async (req, res) => {
  const user = await User.findOne(
      {_id: req.user._id},
      {password: 0, role: 0, __v: 0},
  );

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  res.status(200).json({user});
};

module.exports.deleteUser = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  await User.findByIdAndRemove(req.user._id);

  res.status(200).json({message: 'User was deleted successfully'});
};

module.exports.changePassword = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const {oldPassword, newPassword} = req.body;

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Passwords do not match'});
  }

  const hashPassword = await bcrypt.hash(newPassword, 10);

  await User.updateOne(user, {
    password: hashPassword,
  });

  res.status(200).json({message: 'Password was change'});
};
