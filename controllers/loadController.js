const {TRUCK_TYPES} = require('../config');
const User = require('../models/user');
const Truck = require('../models/truck');
const Load = require('../models/load');

module.exports.addLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  if (user.role !== 'SHIPPER') {
    return res.status(400).json({message: 'User is not a shipper'});
  }

  const {name, payload, pickupAddress, deliveryAddress, dimensions} = req.body;

  const logs = [
    {
      message: `Load assigned to driver with id ${user._id}`,
    },
  ];

  const load = new Load({
    name,
    created_by: user._id,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    logs,
  });

  await load.save();

  res.status(200).json({message: 'Load created successfully'});
};

module.exports.getUserLoads = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const {status, offset = 0, limit = 10} = req.query;
  let loads;

  if (status) {
    loads = await Load.find({
      created_by: req.user._id, status},
    null,
    {limit: +limit, skip: +offset});
  } else {
    loads = await Load
        .find({created_by: req.user._id}, null, {limit: +limit, skip: +offset});
  }

  res.status(200).json({
    message: 'Success',
    loads,
  });
};

module.exports.getUserLoadById = async (req, res) => {
  const id = req.params.id;
  const load = await Load.findById(id, {_v: 0}).exec();

  res.status(200).json({load});
};

module.exports.updateUserLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    return res.status(400).json({message: 'The load does not exist'});
  }

  if (load.created_by != req.user._id || req.user.role === 'DRIVER') {
    return res.status(403).json({message: 'Access denied'});
  }

  if (load.status !== 'NEW') {
    return res.status(400).json({
      message: 'Cannot update info about the load. Its status is not new.',
    });
  }

  const update = {
    name: req.body.name || load.name,
    payload: req.body.payload || load.payload,
    pickup_address: req.body.pickup_address || load.pickup_address,
    delivery_address: req.body.delivery_address || load.delivery_address,
    dimensions: {
      width: req.body.dimensions.width || load.dimensions.width,
      length: req.body.dimensions.length || load.dimensions.length,
      height: req.body.dimensions.height || load.dimensions.height,
    },
  };

  await Load.findByIdAndUpdate(req.params.id, update);

  res.status(200).json({
    message: 'Load information updated successfully.',
  });
};

module.exports.postUserLoad = async (req, res) => {
  const load = await Load.findById(req.params.id);
  const newLogs = load.logs;

  if (!load) {
    return res.status(400).json({message: 'The load does not exist'});
  }

  if (load.status !== 'NEW') {
    return res.status(400).json({
      message: 'Cannot post the load. Its status is not new.',
    });
  }

  load.status = 'POSTED';
  await load.save();

  const truckTypes = TRUCK_TYPES.filter(
      (item) =>
        item.payload >= load.payload &&
            item.dimensions.length >= load.dimensions.length &&
            item.dimensions.width >= load.dimensions.width &&
            item.dimensions.height >= load.dimensions.height,
  ).map((item) => item.type);

  const truck = await Truck.findOne({
    status: 'IS',
    type: {
      $in: truckTypes,
    },
  }).exec();

  if (!truck) {
    newLogs.push({
      message: 'Load posted successfully.',
      driver_found: false,
      time: new Date(),
    });
    load.logs = newLogs;
    load.status = 'NEW';
    await load.save();

    return res.status(200).json({
      message: `Load posted successfully`,
      driver_found: true,
    });
  }

  truck.status = 'OL';

  const driverId = truck.assigned_to;

  newLogs.push({
    message: 'Load posted successfully',
    driver_found: true,
    time: new Date(),
  });

  load.status = 'ASSIGNED';
  load.assigned_to = driverId;
  load.state = 'En route to Pick Up';
  load.logs = newLogs;

  await load.save();
  await truck.save();

  res.status(200)
      .json({message: 'Load posted successfully', driver_found: true});
};

module.exports.getActiveLoad = async (req, res) => {
  const load = await Load.find({assigned_to: req.user._id, status: 'ASSIGNED'});

  if (!load) {
    return res.status(400).json({message: 'The load not found'});
  }

  console.log(load);

  res.status(200).json({load: load[0]});
};

module.exports.changeLoadState = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const load = await Load.findOne({
    assigned_to: user._id,
  });

  if (!load) {
    return res.status(400).json({message: 'The load does not exist'});
  }

  const newLogs = load.logs;
  let message;
  let state;

  switch (load.state) {
    case 'Ready to Pick Up':
      state = 'En route to Pick Up';
      message = 'Load En route to Pick Up';
      break;
    case 'En route to Pick Up':
      state = 'Arrived to Pick Up';
      message = 'Load Arrived to Pick Up';
      break;
    case 'Arrived to Pick Up':
      state = 'En route to delivery';
      message = 'Load En route to delivery';
      break;
    case 'En route to delivery':
      state = 'Arrived to delivery';
      message = 'Load arrived to delivery';
      newLogs.push({
        message: message,
        time: new Date(),
      });
      await Load.findByIdAndUpdate(load._id, {
        $set: {
          state: state,
          status: 'SHIPPED',
          logs: newLogs,
        },
      });

      await Truck.findOneAndUpdate(
          {
            assigned_to: req.user._id,
          },
          {$set: {status: 'IS', assigned_to: null}},
      );

      return res.status(200).json({message: `Load was successfully shipped`});
    default:
      return;
  }

  if (state) {
    load.logs.push({
      message: `Load state changed to ${state}`,
      time: new Date(),
    });
    await Load.findByIdAndUpdate(load._id, {state: state, logs: load.logs});

    res.status(200).json({message: `Load state changed to ${state}`});
  }
};

module.exports.deleteLoadById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    return res.status(400).json({message: 'The load does not exist'});
  }

  await Load.findByIdAndRemove(req.params.id);

  res.status(200).json({message: 'Load removed successfully!'});
};

module.exports.getShippingInfo = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    return res.status(400).json({message: 'The load does not exist'});
  }

  const truck = await Truck.findOne({assigned_to: load.assigned_to});

  res.status(200).json({load, truck});
};
