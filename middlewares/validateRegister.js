const Joi = require('joi');

module.exports.validateRegister = async (req, res, next) => {
  try {
    const schema = Joi.object({
      email: Joi.string().email({minDomainSegments: 2}),
      password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
      role: Joi.string().valid('DRIVER', 'SHIPPER'),
    });

    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({message: `${err.details[0].message}`});
  }
};
