const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config.js');

module.exports.checkAuth = async (req, res, next) => {
  try {
    const header = req.headers['authorization'];

    if (!header) {
      return res.status(401).json({
        message: 'No Authorization http header found!',
      });
    }

    const [tokenType, token] = header.split(' ');

    console.log(`TYPE: ${tokenType}, Token: ${token}`);

    if (!token) {
      return res.status(401).json({message: 'No JWT http token found!'});
    }

    req.user = jwt.verify(token, JWT_SECRET);
    next();
  } catch (error) {
    return res.status(500).json({message: 'Invalid token'});
  }
};
