const Load = require('../models/load');
const Truck = require('../models/truck');

module.exports = async (loadId) => {
  const load = await Load.findById(loadId);

  const truck = await Truck.where('assignedTo')
      .ne(null)
      .where('status')
      .equals('IS')
      .where('payload')
      .gt(load.payload)
      .where('width')
      .gt(load.dimensions.width)
      .where('length')
      .gt(load.dimensions.length)
      .where('height')
      .gt(load.dimensions.height)
      .findOne();

  const updateLogs = [...load.logs];

  if (!truck) {
    updateLogs.push({
      message: 'Changed status to NEW. There is no appropriate truck',
      time: Date.now(),
    });

    await Load.findByIdAndUpdate(loadId, {
      status: 'NEW',
      logs: updateLogs,
    });

    return {
      state: 'No truck',
    };
  }

  await Truck.findByIdAndUpdate(truck._id, {status: 'OL'});

  updateLogs.push({
    message: 'Load is assigned. Driver is en route',
    time: Date.now(),
  });

  await Load.findByIdAndUpdate(loadId, {
    status: 'ASSIGNED',
    state: 'En route to Pick Up',
    logs: updateLogs,
    assigned_to: truck.assigned_to,
  });

  return {
    state: 'En route',
    driverId: truck.assigned_to,
  };
};
