const express = require('express');
const auth = require('../controllers/authController');
const {asyncWrapper} = require('../helpers/helpers');
const {validateRegister} = require('../middlewares/validateRegister');
const router = express.Router();

router.post('/register',
    asyncWrapper(validateRegister),
    asyncWrapper(auth.register));
router.post('/login', asyncWrapper(auth.login));
router.post('/forgot_password', asyncWrapper(auth.forgotPassword));

module.exports = router;
