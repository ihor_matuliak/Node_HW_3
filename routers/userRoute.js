const express = require('express');
const router = express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {checkAuth} = require('../middlewares/authMiddleware');
const user = require('../controllers/userController');

router.get('/me', asyncWrapper(checkAuth), asyncWrapper(user.getUserInfo));
router.delete('/me', asyncWrapper(checkAuth), asyncWrapper(user.deleteUser));

router.patch('/me/password',
    asyncWrapper(checkAuth),
    asyncWrapper(user.changePassword));

module.exports = router;
