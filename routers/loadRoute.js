const express = require('express');
const {asyncWrapper} = require('../helpers/helpers');
const {checkAuth} = require('../middlewares/authMiddleware');
const load = require('../controllers/loadController');
const router = express.Router();

router.get('/', asyncWrapper(checkAuth), asyncWrapper(load.getUserLoads));
router.post('/', asyncWrapper(checkAuth), asyncWrapper(load.addLoad));

router.get('/active',
    asyncWrapper(checkAuth),
    asyncWrapper(load.getActiveLoad));

router.patch('/active/state',
    asyncWrapper(checkAuth),
    asyncWrapper(load.changeLoadState));

router.get('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(load.getUserLoadById));

router.put('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(load.updateUserLoad));

router.delete('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(load.deleteLoadById));

router.post('/:id/post',
    asyncWrapper(checkAuth),
    asyncWrapper(load.postUserLoad));

router.get('/:id/shipping_info',
    asyncWrapper(checkAuth),
    asyncWrapper(load.getShippingInfo));

module.exports = router;
