const express = require('express');
const truck = require('../controllers/truckController');
const {asyncWrapper} = require('../helpers/helpers');
const {checkAuth} = require('../middlewares/authMiddleware');
const router = express.Router();

router.post('/', asyncWrapper(checkAuth), asyncWrapper(truck.addTruck));

router.get('/', asyncWrapper(checkAuth), asyncWrapper(truck.getTruck));

router.get('/:id', asyncWrapper(checkAuth), asyncWrapper(truck.getTruckById));

router.put('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(truck.updateTruckById));

router.delete('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(truck.deleteTruckById));

router.post('/:id/assign',
    asyncWrapper(checkAuth),
    asyncWrapper(truck.assignTruckById));

module.exports = router;
