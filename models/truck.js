const {Schema, model, Types} = require('mongoose');

const truckSchema = new Schema({
  name: {
    type: String,
    default: 'Truck',
  },
  created_by: {
    type: Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: Types.ObjectId,
    ref: 'User',
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
  width: {type: Number, required: true, min: 0, max: 1000},
  length: {type: Number, required: true, min: 0, max: 1000},
  height: {type: Number, required: true, min: 0, max: 1000},
  payload: {type: Number, required: true, min: 0, max: 5000},
});

module.exports = model('Truck', truckSchema);
