const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {type: String, required: true, enum: ['DRIVER', 'SHIPPER']},
  created_date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = model('User', userSchema);
